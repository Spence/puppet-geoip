# GeoIP Puppet Module for Boxen

[![Build Status](https://travis-ci.org/boxen/puppet-geoip.png)](https://travis-ci.org/boxen/puppet-geoip)

Install the [GeoIP](http://www.maxmind.com/en/geolocation_landing)
geolocation library.

## Usage

```puppet
include geoip
```

## Required Puppet Modules

* `boxen`

## Development

Write code. Run `script/cibuild` to test it. Check the `script`
directory for other useful tools.
